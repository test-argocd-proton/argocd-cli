FROM centos:8

# change these when new versions are out
ARG JQ_VERSION=1.6
ARG YQ_VERSION=v4.6.1
ARG KUBECTL_VERSION=v1.19.4
ARG HELM_VERSION=v3.4.1

RUN yum install -y -d1 git wget gettext

# install jq
RUN curl -LO "https://github.com/stedolan/jq/releases/download/jq-${JQ_VERSION}/jq-linux64" && \
  mv jq-linux64 /usr/local/bin/jq && chmod +x /usr/local/bin/jq

# install yq
RUN curl -LO https://github.com/mikefarah/yq/releases/download/${YQ_VERSION}/yq_linux_amd64 && \
  mv yq_linux_amd64 /usr/local/bin/yq && chmod +x /usr/local/bin/yq


# install kubectl
RUN curl -LO "https://storage.googleapis.com/kubernetes-release/release/${KUBECTL_VERSION}/bin/linux/amd64/kubectl" && \
  mv kubectl /usr/local/bin/kubectl && chmod +x /usr/local/bin/kubectl

# install helm and add atlas-charts repo
RUN curl https://get.helm.sh/helm-${HELM_VERSION}-linux-amd64.tar.gz -O && \
  tar zxf helm-${HELM_VERSION}-linux-amd64.tar.gz && \
  mv linux-amd64/helm /usr/local/bin/helm && \
  rm -rf linux-amd64 && rm -rf helm-*.tar.gz
